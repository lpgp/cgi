#!/usr/bin/env nextflow

// Enable DSL 2 syntax
nextflow.enable.dsl = 2

// Import modules 
include { 
	NEWCPGREPORT;
	CPGLH;
	OBSEXP;
	G4HUNTER
} from "./modules/modules.nf"

// output directory
file("${params.out_dir}").mkdirs()

// main pipeline logic
workflow {

	// input
	input=Channel.fromPath(params.input)
		.splitCsv(header:true, sep:',')
    	.map {
        	row -> tuple(
            	row.ID,
				file(row.target,checkIfExists: true),
        	)
    	}

	// newcpgreport
	if(params.newcpgreport){

        // split
		split=input.map{it[1].splitFasta(by:1, file:it[0], decompress:true)}

		// compute
		NEWCPGREPORT(
			split
		)
	}

	// cpglh
	if(params.cpglh){

        // split
		split=input.map{it[1].splitFasta(by:1, file:it[0], decompress:true)}

		// compute
		CPGLH(
			split
		)
	}

	// ObsExp
	if(params.obsexp){

		// compute
		OBSEXP(
			input
		)
	}

		// ObsExp
	if(params.G4hunter){

		// compute
		G4HUNTER(
			input
		)
	}
}
