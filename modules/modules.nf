#!/usr/bin/env nextflow

// newcpgreport
process NEWCPGREPORT {

	input:
	path(seq)

	script:
	"""
	id=\$(head -n 1  ${seq} | sed 's/ .*//g;s/>//g')

	newcpgreport \
		-sequence ${seq} \
		-window ${params.window} \
    	-shift ${params.shift} \
    	-minlen ${params.minlen} \
    	-minoe ${params.minoe} \
    	-minpc ${params.minpc} \
		-outfile "file.newcpgreport"

	paste \
		<(printf %s "\$(grep "FT *CpG island" file.newcpgreport | sed "s/.* //g;s/\\.\\./\\t/g;s/^/\$id\\t/")") \
		<(printf %s "\$(grep "FT */size" file.newcpgreport | sed 's/.*=//g')") \
		<(printf %s "\$(grep "FT */Sum" file.newcpgreport | sed 's/.*=//g')") \
		<(printf %s "\$(grep "FT */Percent" file.newcpgreport | sed 's/.*=//g')") \
		<(printf %s "\$(grep "FT */ObsExp" file.newcpgreport | sed 's/.*=//g')") \
	>> "${params.out_dir}/${seq}_\${id}.bed"

	# seqnames start end size sum_CG percent_CG ObsExp"
	"""
}

// cpglh
process CPGLH {

	input:
	path(seq)

	script:
	"""
	id=\$(head -n 1  ${seq} | sed 's/ .*//g;s/>//g')
	cpg_lh ${seq} >> "${params.out_dir}/${seq}_\${id}.bed"

	#  seqnames start end mx ncpg gc ncpg_ngpc ObsExp
	"""
}

// ObsExp
process OBSEXP {

    publishDir "${params.out_dir}", mode: 'copy', overwrite: true

	input:
	tuple val(ID), path(seq)

	output:
	path '*.txt.gz'

	script:
	"""
    zcat ${seq} | seqkit sliding -s ${params.obsexp_shift} -W ${params.obsexp_window} | tee \
	>(grep ">" | sed 's/>//g'  | sort | gzip > ref.gz) \
    >(seqkit locate --bed -P -G -p C | cut -f1 | uniq -c | sort -k2 | gzip > C.count.gz) \
    >(seqkit locate --bed -P -G -p G | cut -f1 | uniq -c | sort -k2 | gzip > G.count.gz) | \
    seqkit locate --bed -P -G -p CG | cut -f1 | uniq -c | sort -k2 | gzip > CG.count.gz

	awk 'BEGIN{FS="_sliding:|-| ";print "chr start end C G CG ObsExp"}{print \$1 ,\$2,\$3,\$4,\$5,\$6,(\$6*(\$3-\$2+1))/(\$4*\$5)}' <<<\$( \
		join -a 1 -1 1 -2 2 -e 0 -o 1.1 2.1 <(zcat ref.gz) <(zcat C.count.gz) | \
		join -a 1 -1 1 -2 2 -e 0 -o 1.1 1.2 2.1 - <(zcat G.count.gz) | \
		join -a 1 -1 1 -2 2 -e 0 -o 1.1 1.2 1.3 2.1 - <(zcat CG.count.gz)) | \
	    gzip >  "${ID}_s${params.obsexp_shift}_w${params.obsexp_window}.txt.gz"
	"""
}

// ObsExp
process G4HUNTER {

    publishDir "${params.out_dir}", mode: 'copy', overwrite: true

	input:
	tuple val(ID), path(seq)

	output:
	path '*.txt.gz'

	script:
	"""
	zcat ${seq} >seq.fa
	g4hunter.awk window_size=${params.G4hunter_window} step_size=${params.G4hunter_shift} threshold=0 seq.fa | \
	gzip >"${ID}_s${params.G4hunter_shift}_w${params.G4hunter_window}_G4.txt.gz"
	"""
}
