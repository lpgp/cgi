#!/usr/bin/awk -f

BEGIN {
    if (window_size == "") window_size = 20
    if (threshold == "") threshold = 1.5
    if (step_size == "") step_size = 1
    seq = ""
    header = "unknown"
    print "chr\tstart\tend\tscore\tseq"
}

# Process FASTA header
/^>/ {
    if (seq != "") process_sequence()
    header = substr($0, 2)
    seq = ""
    next
}

# Collect sequence data
{
    seq = seq $0
}

END {
    if (seq != "") process_sequence() 
}

function process_sequence() {
    
    # Remove whitespace and convert to uppercase
    gsub(/[[:space:]]/, "", seq)
    seq = toupper(seq)
    
    len = length(seq)
    
    # Calculate scores for each nucleotide
    for (i = 1; i <= len; i++) {
        base = substr(seq, i, 1)
        if (base == "G") {
            score = 1
            # Check for consecutive G's
            for (j = 1; j <= 3; j++) {
                if (substr(seq, i+j, 1) == "G") score++
                else break
            }
        } else if (base == "C") {
            score = -1
            # Check for consecutive C's
            for (j = 1; j <= 3; j++) {
                if (substr(seq, i+j, 1) == "C") score--
                else break
            }
        } else {
            score = 0
        }
        scores[i] = score
    }
    
    # Calculate window scores using the specified step size
    for (i = 1; i <= len - window_size + 1; i += step_size) {
        sum = 0
        for (j = i; j < i + window_size; j++) {
            sum += scores[j]
        }
        avg = sum / window_size
        if (avg >= threshold || avg <= -threshold) {
            window_seq = substr(seq, i, window_size)
            printf "%s\t%d\t%d\t%.2f\t%s\n", header, i, i+window_size-1, avg, window_seq
            
        }
    }
    
    # Clear scores array for next sequence
    delete scores
}
