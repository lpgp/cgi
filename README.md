# cgi v1.2

**cgi** workflow , which agree to FAIR principles , was built in Nexflow dsl2 language, with singularity container for used softwares, optimized in terms of computing resources (cpu, memory), and its use on a informatic farm with a slurm scheduler.

- CpG islands defined using [EMBOSS newcpgreport](https://www.bioinformatics.nl/cgi-bin/emboss/help/newcpgreport)
- CpG islands defined using [UCSC cpg_lh](http://genome.ucsc.edu/cgi-bin/hgTrackUi?hgsid=1136815349_vADDCkvwSdu3g8CimOykIvSHZHar&db=rn3&c=chr5&g=cpgIsland)
- window C, G, CpG counts and CpG Observed/Expected
- G4 quadruplex prediction (G4hunter algorithm)

## Install CGI flow and build singularity image

Clone CGI git and build local singularity image (with system admin rights) based on the provided singularity definition file.

```bash
git clone https://forgemia.inra.fr/lpgp/cgi.git
sudo singularity build ./cgi/singularity/CGI.sif ./cgi/singularity/CGI.def
```

## Usage example

design.csv file must have *ID* and *target* header and write with comma separator.

|ID|target|
|:-|:-|
|A|/path/to/targetA.fa.gz|
|B|/path/to/targetB.fa.gz|
|C|/path/to/targetC.fa.gz|

###  ENBOSS newcpgreport

```bash
#!/bin/bash
#SBATCH -J CGI
#SBATCH --mem=10GB
#SBATCH -p unlimitq
module load containers/singularity/3.9.9
module load bioinfo/Nextflow/23.04.3
nextflow run /work/project/lpgp/Nextflow/cgi/ \
-profile slurm \
--input "design.csv" \
--newcpgreport \
--window 100 \
--shift 1 \
--minlen 200 \
--minoe 0.6 \
--minpc 50 \
--cpglh \
--out_dir "${PWD}/results"
```

### UCSC cpg_lh

```bash
#!/bin/bash
#SBATCH -J CGI
#SBATCH --mem=10GB
#SBATCH -p unlimitq
module load containers/singularity/3.9.9
module load bioinfo/Nextflow/21.10.6
nextflow run /work/project/lpgp/Nextflow/cgi/ \
-profile slurm \
--input "design.csv" \
--cpglh \
--out_dir "${PWD}/results"
```

### CpG Obs/Exp sliding windows

```bash
#!/bin/bash
#SBATCH -J CGI
#SBATCH --mem=10GB
#SBATCH -p unlimitq
module load containers/singularity/3.9.9
module load bioinfo/Nextflow/21.10.6
nextflow run /work/project/lpgp/Nextflow/cgi/ \
-profile slurm \
--input "design.csv" \
--obsexp \
--obsexp_shift 200 \
--obsexp_window 200 \
--out_dir "${PWD}/results"
```

### G4 sliding windows

```bash
#!/bin/bash
#SBATCH -J CGI
#SBATCH --mem=10GB
#SBATCH -p unlimitq
module load containers/singularity/3.9.9
module load bioinfo/Nextflow/21.10.6
nextflow run /work/project/lpgp/Nextflow/cgi/ \
-profile slurm \
--input "design.csv" \
--G4hunter \
--G4hunter_shift 200 \
--G4hunter_window 200 \
--out_dir "${PWD}/results"
```

## Defaults parameters

Please refer to [EMBOSS newcpgreport](https://www.bioinformatics.nl/cgi-bin/emboss/help/newcpgreport), and [UCSC cpg_lh](http://genome.ucsc.edu/cgi-bin/hgTrackUi?hgsid=1136815349_vADDCkvwSdu3g8CimOykIvSHZHar&db=rn3&c=chr5&g=cpgIsland) for complete arguments explanation.

```bash
# sequences input
input = false

# newcpgreport options
newcpgreport = false
window = 100
shift = 1
minlen = 200
minoe = 0.6
minpc = 50

# cpg_lh
cpglh = false

# ObsExp
obsexp = false
obsexp_shift = 200
obsexp_window = 200

// G4hunter
G4hunter = false
G4hunter_shift = 200
G4hunter_window = 200

# save directory
out_dir = "${PWD}/results"
```
